from genericpath import isdir
import os
import sys
import platform
import requests
import json
import datetime
import time
import argparse
from pathlib import Path
from math import ceil
from dbman import DBManager
from imagemanipulator import ImageManipulator
from helpstrings import *
# https://enigmatico.gitlab.io/enigmaticos-python-modules/
from Debug import Debug
from ConfigManager import ConfigManager

ptitle = """
   ______                    ______                                    
  (, /    )                 (, /    )              /)         /)       
    /---(  ________           /    / ____   ___   // ____   _(/  _  __ 
 ) / ____)(_)(_)/ (_(_(_    _/___ /_(_) (_(/ / (_(/_(_)(_(_(_(__(/_/ (_
(_/ (                     (_/___ /                                     
                                                                       
                         By Enigmatico - v1.3
                         
"""

USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:104.0) Gecko/20100101 Firefox/104.0"
CONFIG = None

def strtobool(text: str):
    return True if text.lower() == "true" else False

def buildUrl(tags, service = "gelbooru.com", page='dapi', search='post', query='index', json='1', pageid='0', limit='100'):
    if service == "gelbooru.com" or service == "safebooru.org":
        url = f'https://{service}/index.php'
        url = url + '?page=' + page
        url = url + '&s=' + search
        url = url + '&q=' + query
        url = url + '&json=' + json
        url = url + '&tags=' + tags
        url = url + '&limit=' + str(limit)
        url = url + '&pid=' + str(pageid)
        
    elif service == "danbooru.donmai.us":
        if page == 'dapi':
            page = 0
        url = f'https://danbooru.donmai.us/posts.json'
        url = url + '?page=' + str(pageid)
        url = url + '&tags=' + tags
        url = url + '&limit=' + str(limit)
    elif service == "yande.re":
        if page == 'dapi':
            page = 0
        url = f'https://yande.re/post.json'
        url = url + '?tags=' + tags
        url = url + '&limit=' + str(limit)
        url = url + "&api_version=2"
        url = url + '&page=' + str(pageid)
    return url

def printsl(txt: str):
    print(txt, end='\r')
    sys.stdout.flush()

class GelDanDownloader():
    tags = {}
    
    def __init__(self):
        self.db = DBManager(dbpath=CONFIG.getConfig('DATABASEPATH'))
    
    def getTags(self):
        try:
            if not os.path.exists(CONFIG.getConfig('CACHEPATH')):
                Path(CONFIG.getConfig('CACHEPATH')).mkdir(parents=True, exist_ok=True)
            print(f"Updating tags, this might take a little while...")
            page = 0
            ttags = 0
            pages = 1
            lpage = 0
            
            if os.path.isfile(f"{CONFIG.getConfig('CACHEPATH')}/lastpage"):
                with open(f"{CONFIG.getConfig('CACHEPATH')}/lastpage", "r") as f:
                    lpage = int(f.read())
                    
            if os.path.isfile(f"{CONFIG.getConfig('CACHEPATH')}/progress"):
                with open(f"{CONFIG.getConfig('CACHEPATH')}/progress", "r") as f:
                    self.tags = json.loads(f.read())

            while page < pages:
                page = page if lpage == 0 else lpage
                lpage = 0
                resp = requests.get(f"https://gelbooru.com/index.php?page=dapi&s=tag&q=index&json=1&pid={page}", headers={"User-Agent": USERAGENT})
                if resp.status_code == 200:
                    try:
                        #print(f"Resp: {resp.text}")
                        data = json.loads(resp.text)
                        
                        if ttags == 0:
                            ttags = data["@attributes"]["count"]
                            pages = ceil(data["@attributes"]["count"] / 100)
                            #print(f"Tags: {ttags}, Pages: {pages}")
                        
                        for tag in data["tag"]:
                            self.tags[tag["name"]] = tag
                            printsl(f"{len(self.tags)}/{ttags} ({ceil((len(self.tags) * 100)/ttags)}%)")

                        page += 1
                        with open(f"{CONFIG.getConfig('CACHEPATH')}/lastpage", "w") as f:
                            f.write(str(page))
                        with open(f"{CONFIG.getConfig('CACHEPATH')}/progress", "w") as f:
                            f.write(json.dumps(self.tags))
                    except json.JSONDecodeError as exc:
                        Debug.debugException(exc, True, None, True, ['data', 'page', 'lpage', 'resp'])
                        return -3
                else:
                    print(f"Unable to retrieve tags (Response code: {resp.status_code}")
                    return -1
                time.sleep(0.20)
                
            if len(self.tags) > 0:
                    with open(f"{CONFIG.getConfig('CACHEPATH')}/tags.json", "w") as f:
                        f.write(json.dumps(self.tags))
            if os.path.isfile(f"{CONFIG.getConfig('CACHEPATH')}/lastpage"):
                os.remove(f"{CONFIG.getConfig('CACHEPATH')}/lastpage")
            if os.path.isfile(f"{CONFIG.getConfig('CACHEPATH')}/progress"):
                os.remove(f"{CONFIG.getConfig('CACHEPATH')}/progress")
        except Exception as exc:
            Debug.debugException(exc, True, None, True, ['data', 'page', 'lpage', 'resp'])
    
    def download(self, search_tags: str, output_folder: str = None, compress: bool = False, **kwargs):
        failed = []
        if output_folder is None:
            output_folder = f"{CONFIG.getConfig('COLLECTIONPATH')}/default"
        durl = buildUrl(tags = search_tags,
                        service = kwargs["service"] if "service" in kwargs else 'gelbooru.com',
                        page = kwargs["page"] if "page" in kwargs else 'dapi',
                        search = kwargs["search"] if "search" in kwargs else 'post',
                        query = kwargs["query"] if "query" in kwargs else 'index',
                        pageid = kwargs["pageid"] if "pageid" in kwargs else 0,
                        limit = kwargs["limit"] if "limit" in kwargs else 100)
        
        print(f"Request: {durl}")
        
        time.sleep(0.15)
        res = requests.get(url = durl, headers={"User-Agent": USERAGENT})
        if res.status_code == 200:
            if len(res.text) == 0:
                print(f"[WARNING] Empty response from the server.")
                return -3
            try:
                data = json.loads(res.text)
                if 'posts' in data:
                    data = data["posts"]
                if len(data) == 0:
                    print(f"[WARNING] Empty response from the server.")
                    return -3
                if "service" in kwargs:
                    if kwargs["service"] == 'gelbooru.com':
                        if 'post' not in data:
                            print(f"Your query returned no results.")
                            return -3
                        data = data["post"]
                
                if not os.path.exists(f"{output_folder}"):
                    os.mkdir(f"{output_folder}")
                
                for (i,post) in enumerate(data):
                    if 'service' in kwargs:
                        if kwargs["service"] == 'safebooru.org':
                            post["file_url"] = f"https://safebooru.org//images/{post['directory']}/{post['image']}"
                        if kwargs["service"] == 'danbooru.donmai.us' or kwargs["service"] == 'yande.re':
                            post["image"] = post['file_url'].split('/')[-1] if 'file_url' in post else 'None'
                        if kwargs["service"] == 'yande.re':
                            post["created_at"] = datetime.datetime.utcfromtimestamp(post["created_at"]).strftime('%Y-%m-%d %H:%M:%S')
                            
                    if 'file_url' not in post:
                        print(f"[ERROR] file_url not in post. Skipping...")
                        post["failed_reason"] = "file_url not in post."
                        failed.append(post)
                        continue
                    if self.db.postExists(post):
                        print(f"Skipping {str(i)}: {post['file_url']} (Already exists)")
                        continue
                    print(f"Downloading {str(i)}: {post['file_url']}...", end=' ')
                    
                    try:
                        time.sleep(0.15)
                        fres = requests.get(url = post["file_url"], headers={"User-Agent": USERAGENT}, stream=True)
                        if fres.status_code == 200:
                            total_length = fres.headers['Content-length']
                            with open(f'{output_folder}/{post["image"]}', 'wb') as f:
                                for chunk in fres.iter_content(chunk_size=16384):
                                    f.write(chunk)
                            post["path"] = f'{output_folder}/{post["image"]}'
                            if compress:
                                cr = ImageManipulator.compress(f'{output_folder}/{post["image"]}')
                                if cr is not None:
                                    post["compressed_name"] = cr[0]
                                    post["compressed_size"] = cr[1]
                                    post["compressed_width"] = cr[3][0]
                                    post["compressed_height"] = cr[3][1]
                                    post["compressed_hash"] = cr[2]
                                    post["path"] = f'{output_folder}/{cr[0]}'
                            post["provider"] = kwargs['service'] if 'service' in kwargs else 'gelbooru.com'
                            self.db.insertPost(post)
                            print("100%")
                        else:
                            print(f"[ERROR] (HTTP {str(fres.status_code)})")
                            post["failed_reason"] = f"Failed to download file. Response code: {str(fres.status_code)}"
                            failed.append(post)
                    except requests.ConnectionError as exc:
                        print(f"[ERROR] (ConnectionError)")
                        post["failed_reason"] = f"Failed to download file (ConnectionError)"
                        failed.append(post)
                        continue
                    except requests.ConnectTimeout as exc:
                        print(f"[ERROR] (ConnectionTimeout)")
                        post["failed_reason"] = f"Failed to download file (ConnectionTimeout)"
                        failed.append(post)
                        continue
                    except requests.HTTPError as exc:
                        print(f"[ERROR] (HTTPError)")
                        post["failed_reason"] = f"Failed to download file (HTTPError)"
                        failed.append(post)
                        continue
                    except requests.RequestException as exc:
                        Debug.debugException(exc, True, None, True, ['durl'])
                        #print(f"[ERROR] (RequestException)")
                        post["failed_reason"] = f"Failed to download file (RequestException)"
                        failed.append(post)
                        continue
                    except Exception as exc:
                        Debug.debugException(exc,True, None, True, ['durl', 'post', 'fres', 'cr'])
                        #print(f"[ERROR] ({str(exc)}")
                        post["failed_reason"] = f"Failed to download file ({str(exc)})"
                        failed.append(post)
                        continue
                if len(failed) > 0:
                    if not os.path.exists(CONFIG.getConfig('FAILED')):
                        os.mkdir(CONFIG.getConfig('FAILED'))
                    with open(f'{CONFIG.getConfig("FAILED")}/failed-{datetime.datetime.now().strftime("%Y%m%d%H%M%S")}.json', 'w') as f:
                        f.write(json.dumps(failed))
                    print(f"Some files failed to download. Failed entries have been dumped into {CONFIG.getConfig('FAILED')}/failed-{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}.json")
                    return len(data)
                return 0
            except json.JSONDecodeError as exc:
                print(f"The server returned something that is not JSON. Either there was an error, your query format is wrong, or this page has no results.")
                Debug.debugLog(f"URL: {durl}\nResponse: {res.text}", "json.JSONDecodeError while parsing post list")
                return -3
            except Exception as exc:
                Debug.debugException(exc, True, None, True, ['durl', 'kwargs', 'res', 'data', 'post', 'failed', 'compress'])
                print(f"Something went wrong:\nRequest: {durl}\nException: {exc}")
                return -2
        else:
            print(f"Failed to connect to {kwargs['service'] if 'service' in kwargs else 'gelbooru.com'} - Server status code {str(res.status_code)}")
            return -1

def doDownload(service: str, **kwargs):
    for i in range(int(pid_),int(kwargs['pagelimit'])):
        print(f"Downloading page {str(i+1)}...")
        r = -1
        rets = 0
        while r == -1 and rets < 5:
            r = dl.download('+'.join(kwargs['tags'].split(' ')),kwargs['folder'],kwargs['compress'],service=service, pageid=i, limit=kwargs['limit'])
            rets += 1
            if r == -1:
                print("Retrying in 30 seconds...")
                time.sleep(30.0)
            elif r == -2:
                print("Abandoned after a connection error.")
                return
            elif r == -3:
                print("Job's done")
                return
    

if __name__ == "__main__":

    defaultConfigDir = "./config.json"
    if platform.system() == 'Windows':
        appdata = f"{os.getenv('APPDATA')}\\BooruDownloader"
        if os.path.isdir(appdata) == True:
            if os.path.isfile(f"{appdata}\\config.json"):
                defaultConfigDir = f"{appdata}\\config.json"
    else:
        appdata = f"~/.boorudownloader/config.json"
        if os.path.isdir(appdata) == True:
            if os.path.isfile(f"{appdata}/config.json"):
                defaultConfigDir = f"{appdata}/config.json"
        else:
            appdata = f"/var/boorudownloader"
            if os.path.isdir(appdata) == True:
                if os.path.isfile(f"{appdata}/config.json"):
                    defaultConfigDir = f"{appdata}/config.json"
            
    parser = argparse.ArgumentParser()
    #parser.add_argument('-h', '--help', action='help',default=argparse.SUPPRESS,help=getLocalizedCmdHelp('CMDHELP'))
    parser.add_argument('-t', '--tags', action='store', nargs='+', help=getLocalizedCmdHelp('CMDTAGS'))
    parser.add_argument('-f', '--folder', action='store', default='default', nargs='+', help=getLocalizedCmdHelp('CMDFOLDER'))
    parser.add_argument('-sv', '--services', action='store', default = "All", nargs='+', help=getLocalizedCmdHelp('CMDSERVICES'))
    parser.add_argument('-s', '--start', action='store', default=0, help=getLocalizedCmdHelp('CMDSTART'))
    parser.add_argument('-p', '--post_per_page', action='store', default=100, help=getLocalizedCmdHelp('CMDPOST_PER_PAGE'))
    parser.add_argument('-l', '--page_limit', action='store', default=100, help=getLocalizedCmdHelp('CMDPAGELIMIT'))
    #parser.add_argument('-ig', '--ignore_tags', action='store', nargs='+') # NOT IMPLEMENTED YET (Is it necessary?)
    parser.add_argument('-co', '--compress', action='store_true', help=getLocalizedCmdHelp('CMDCOMPRESS'))
    parser.add_argument('-ts', '--tag_search', action='store', nargs='+', help=getLocalizedCmdHelp('CMDTAGSEARCH'))
    parser.add_argument('-c', '--config', action='store', nargs='+', default=defaultConfigDir, help=getLocalizedCmdHelp('CMDCONFIG'))
    #parser.add_argument('-q', '--query_db', action='store', nargs='+') # NOT IMPLEMENTED YET (Would be better to have a different program for this)
    parg = parser.parse_args(sys.argv[1:])

    print(f"Configuration file found in {defaultConfigDir}")
    if os.path.isfile(parg.config) == False:
        print(f"ERROR: Configuration file not found in {parg.config}\n")
        print("Use the -c flag to set the path to the configuration file, or", end='')
        if platform.system() == 'Windows':
            print(f" place your configuration file in {os.getenv('APPDATA')}\\BooruDownloader")
        else:
            print(" place your configuration file in one of these locations:")
            print("~/.boorudownloader/config.json")
            print("/var/boorudownloader/config.json")
        print("Then set up your configuration file accordingly. The logger settings can be left by default.")
        sys.exit(1)
    CONFIG = ConfigManager(parg.config.replace('config.json', ''), parg.config)
    
    Debug.set_config({
        "DEBUG_FILE": CONFIG.getConfig("LOGPATH"),
        "MAX_RECURSION": CONFIG.getConfig("LOG_MAX_RECURSION"),
        "WRITE_TO_STDOUT": strtobool(CONFIG.getConfig("LOG_WRITE_TO_STDOUT")),
        "VERBOSE_STDOUT": strtobool(CONFIG.getConfig("LOG_VERBOSE_STDOUT")),
        "ROLLER": strtobool(CONFIG.getConfig("LOG_ROLLER")),
        "ROLLER_LINES": CONFIG.getConfig("LOG_ROLLER_LINES"),
        "DUMP_BYTES": strtobool(CONFIG.getConfig("LOG_DUMP_BYTES")),
        "DUMP_BYTES_LENGTH": CONFIG.getConfig("LOG_DUMP_BYTES_LENGTH")
    })

    if parg.tags is None and parg.tag_search is None:
        parser.print_usage()
        print("Error: One of the following modes must be specified: -t, -ts")
        sys.exit(-1)
        
    if parg.tags is not None and parg.folder == 'default':
        parser.print_usage()
        print("Warning: Output folder was not specified. Using collections/default")
    
    if (parg.tags is not None) and (parg.tag_search is not None):
        parser.print_usage()
        print("Error: Only one of the following modes can be specified: -t, -ts")
        sys.exit(-1)
    
    if parg.tags is not None:
        folder = f"{CONFIG.getConfig('COLLECTIONPATH')}/{str(parg.folder[0])}"
        page = 0
        json_ = 1
        pid_ = parg.start if parg.start is not None else 0
        limit_ = 100 if parg.post_per_page is None else parg.post_per_page
        pagelimit_ = 100 if parg.page_limit is None else parg.page_limit
        tags_ = '+'.join(parg.tags).lower().strip()
        compress_ = parg.compress

        if not os.path.isdir(CONFIG.getConfig('COLLECTIONPATH')):
            Path(CONFIG.getConfig('COLLECTIONPATH')).mkdir(parents=True, exist_ok=True)
        
        print(ptitle)
        
        dl = GelDanDownloader()
        
        if parg.services == "All":
            doDownload("safebooru.org", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
            doDownload("danbooru.donmai.us", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
            doDownload("yande.re", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
            doDownload("gelbooru.com", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
        else:
            if isinstance(parg.services, list):
                for service in parg.services:
                    match service:
                        case 'All':
                            doDownload("safebooru.org", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                            doDownload("danbooru.donmai.us", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                            doDownload("yande.re", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                            doDownload("gelbooru.com", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                        case 'gelbooru':
                            doDownload("gelbooru.com", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                        case 'safebooru':
                            doDownload("safebooru.org", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                        case 'danbooru':
                            doDownload("danbooru.donmai.us", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                        case 'yandere':
                            doDownload("yande.re", compress=compress_, folder=folder, tags = tags_, limit = limit_, pagelimit = pagelimit_)
                        case other:
                            print(f"Unrecognized service: {service}")
    elif parg.tag_search is not None:
        dl = GelDanDownloader()
        if not os.path.isfile(f"{CONFIG.getConfig('CACHEPATH')}/tags.json"):
            dl.getTags()
        else:
            with open(F"{CONFIG.getConfig('CACHEPATH')}/tags.json", "r") as f:
                dl.tags = json.loads(f.read())
        # print(f"Searching for tags containing {parg.tag_search}...")
        for tag in dl.tags.keys():
            if any(tagstr in tag for tagstr in parg.tag_search):
                print(f"{tag}")