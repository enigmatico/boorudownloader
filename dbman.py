import sqlite3
import os
from pathlib import Path

class DBManager():
    _TABLECOLUMNS = ('id', 'source', 'directory', 'hash', 'height', 
                     'image', 'change', 'owner', 'parent_id', 'rating', 
                     'score', 'tags', 'width', 'file_url', 'created_at',
                     'compressed_name', 'compressed_width', 'compressed_height',
                     'compressed_size', 'compressed_hash', 'path', 'provider')
    
    def __init__(self, dbpath: str = "./db"):
        if not os.path.isdir(dbpath):
            Path(dbpath).mkdir(parents=True, exist_ok=True)
        self._conn = sqlite3.connect(f'{dbpath}/gefe.db')
        self._createTable()
    
    def dbquery(self, query):
        self._conn.execute(query)
        self._conn.commit()

    @staticmethod
    def _generateValues(data):
        hashfieldname = 'hash' if 'hash' in data else 'md5'
        tagsfieldname = 'tags' if 'tags' in data else 'tag_string_general'
        source = data['source'] if 'source' in data else 'Unknown'
        creation_date = data['created_at'] if 'created_at' in data else 'Unknown'
        directory = data['directory'] if 'directory' in data else 'Unknown'
        height = data['height'] if 'height' in data else 0
        width = data['width'] if 'width' in data else 0
        image = data['image'] if 'image' in data else data['file_url'].split('/')[-1]
        change = data['change'] if 'change' in data else 0
        owner = data['owner'] if 'owner' in data else 'Unknown'
        vals = str(data['id']) + ', "' + source  + '", "' + directory  + '", "' + data[hashfieldname]  + '", ' + str(height)  + ', "' + image  + '", ' + str(change)  + ', "' + owner
        vals = vals + '", ' + str(data['parent_id'])  + ', "' + data['rating'] + '", ' + str(data['score']) + ', "' + data[tagsfieldname] + '", ' + str(width)
        vals = vals + ', "' + data['file_url'] + '", "' + creation_date + '"'
        vals = vals + ', "' + data['compressed_name'] if 'compressed_name' in data else vals + ', "' + "NULL"
        vals = vals + '", "' + str(data['compressed_width']) if 'compressed_width' in data else vals + '", "' + "NULL"
        vals = vals + '", "' + str(data['compressed_height']) if 'compressed_height' in data else vals + '", "' + "NULL"
        vals = vals + '", "' + str(data['compressed_size']) if 'compressed_size' in data else vals + '", "' + "NULL"
        vals = vals + '", "' + data['compressed_hash'] if 'compressed_hash' in data else vals + '", "' + "NULL"
        vals = vals + '", "' + data['path'] if 'path' in data else vals + '", "' + "Unknown"
        vals = vals + '", "' + data['provider'] if 'provider' in data else vals + '", "' + "Unknown"
        vals = vals + '"'
        
        return vals.replace('"None",', 'null,').replace("None,", 'null,').replace('""', 'null')

    def postExists(self, data):
        cursor = self._conn.cursor()
        cursor.execute('SELECT hash FROM posts WHERE hash="' + data['hash' if 'hash' in data else 'md5'] + '"')
        row = cursor.fetchone()
        if row is None:
            return False
        return True

    def _createTable(self):
        self._conn.execute('CREATE TABLE IF NOT EXISTS posts(' + (','.join(self._TABLECOLUMNS)) + ')')
        self._conn.commit()

    def insertPost(self, data):
        if self.postExists(data):
            print("Already exists: " + str(data['hash']))
            return
        data['tags'] = data['tags'] if 'tags' in data else data['tag_string_general']
        data['hash'] = 'hash' if 'hash' in data else data['md5']
        data['source'] = data['source'] if 'source' in data else 'Unknown'
        data['created_at'] = data['created_at'] if 'created_at' in data else 'Unknown'
        data['directory'] = data['directory'] if 'directory' in data else 'Unknown'
        data['height'] = data['height'] if 'height' in data else 0
        data['width'] = data['width'] if 'width' in data else 0
        data['image'] = data['image'] if 'image' in data else data['file_url'].split('/')[-1]
        data['change'] = data['change'] if 'change' in data else 0
        data['owner'] = data['owner'] if 'owner' in data else 'Unknown'
        self._conn.execute('INSERT INTO posts VALUES (' + DBManager._generateValues(data) + ')')
        self._conn.commit()

