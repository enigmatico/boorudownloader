from io import SEEK_END
from math import floor
from PIL import Image
import os
import hashlib

class ImageManipulator():
    _MAXWIDTH = 2048
    _MAXHEIGHT = 2048
    @staticmethod
    def compress(image: str) -> tuple:
        if image.lower().endswith(('.jpg', '.jpeg', '.png', '.webp')):
            output = image.replace(".png", ".jpg").replace(".webp",".jpg")
            im = Image.open(image).convert('RGB')
            
            width, height = im.size
            
            ratio = min(ImageManipulator._MAXWIDTH / width, ImageManipulator._MAXHEIGHT / height)
            desired = (floor(width * ratio), floor(height * ratio))
            
            imres = im.resize(desired, Image.ANTIALIAS)
            imres.save(output, "JPEG", quality=70, optimize=True)
            
            if image != output:
                os.remove(image)
            d = open(output, 'rb')
            newhash = hashlib.md5(d.read()).hexdigest()
            newsize = d.tell()
            d.close()
            # (output_filename, compressed_size, md5_hash, size(tuple))
            return (output.split('/')[-1], newsize, newhash, desired)
        else:
            print("Not a recognized image format - Will not compress ")
            return None