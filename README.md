# Booru Downloader
## Version 1.0 - By Enigmatico

Python script to download images from different boorus.

To run this program, execute downloader.py with Python (Version 3.9+ required).
If you need a list of parameters to use, use -h/--help.

## Basic usage
This is a script that downloads images (and videos) from three different boorus. Basically gelbooru, danbooru and safebooru. It will also keep the information of your collections in a sqlite database, not only to keep track of your downloads but also to keep a record of their information.

Just place the script and the cache folder (or the precompiled binary bundle) in whatever folder you want. Then place the configuration file in one of their respective default directories:

**Windows**: %APPDATA%\\BooruDownloader\\config.json
**Linux**: /home/<your user name>/.boorudownloader/config.json or /var/boorudownloader/config.json

Then set up your configuration file accordingly, and call the script using Python. Like:

python downloader.py -t <your tags> -f <your output folder>

There are several arguments you can use with the downloader. There are two main ones: -t/--tags (this will specify the tags to search and download), and -ts/--tag_search (searches tags with the specified string). The output of -ts should be grep-able, but tell me if it isn't (I didn't test it).

To download images, specify the tags you want to download with -t. Then specify the output folder name with -f/--folder. The output folder will be created inside a folder called 'collections' in the script folder. Example:

> python downloader.py -t ceres_fauna hololive -f grass

The output produced by -t is not grep-able, be aware of that.

There are a few more arguments to use with -t. For instance, if you want to compress the downloaded images to save a lot of space, you can set the -co/--compress flag. Try the -h/--help command for more arguments.

Tag search is simple, just use -ts and the string you want to search. For instance:

> python downloader.py -ts koyori

The releases with the bundled binaries are in the releases of the project:

https://gitlab.com/enigmatico/boorudownloader/-/releases

Have fun!