import os
import locale
import ctypes
import platform

def getSystemLanguage():
    match platform.system():
        case 'Linux':
            return os.getenv('LANG').split('.')[0].strip().upper()
        case 'Darwin':
            return os.getenv('LANG').split('.')[0].strip().upper()
        case 'Windows':
            kernel32 = ctypes.windll.kernel32
            return locale.windows_locale[ kernel32.GetUserDefaultUILanguage() ]

# If one day I decide to add more languages, I'll implement all the string in a dict and optimize this.
# Until then, bear with this crappy code.
def getLocalizedCmdHelp(cmdhelp: str) -> str:
    match cmdhelp:
        case "HELP":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDHELP_ES
            else:
                return CMDHELP
        case "CMDTAGS":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDTAGS_ES
            else:
                return CMDTAGS
        case "CMDFOLDER":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDFOLDER_ES
            else:
                return CMDFOLDER
        case "CMDSERVICE":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDSERVICES_ES
            else:
                return CMDSERVICES
        case "CMDSTART":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDSTART_ES
            else:
                return CMDSTART
        case "CMDPOST_PER_PAGE":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDPOST_PER_PAGE_ES
            else:
                return CMDPOST_PER_PAGE
        case "CMDPAGELIMIT":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDPAGELIMIT_ES
            else:
                return CMDPAGELIMIT
        case "CMDCOMPRESS":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDCOMPRESS_ES
            else:
                return CMDCOMPRESS
        case "CMDTAGSEARCH":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDTAGSEARCH_ES
            else:
                return CMDTAGSEARCH
        case "CMDCONFIG":
            if getSystemLanguage().split('_')[0].lower() == 'es':
                return CMDCONFIG_ES
            else:
                return CMDCONFIG


# English
CMDHELP = "Show this help message and exit"
CMDTAGS = "List of tags, separated with spaces (example: nanakusa_nazuna yofukashi_no_uta)"
CMDFOLDER = "Name of the output folder (where the downloaded material will be stored). This folder will be created inside the 'collections' folder. Optional (Default: default)"
CMDSERVICES = "List of services to download material from. Can have the following values: All, gelbooru, safebooru, yandere, danbooru (Default: All)"
CMDSTART = "Starting page, this is a numeric value. It will start downloading from this page. (Default: 0)"
CMDPOST_PER_PAGE = "Number of posts per page. From 1 to 100. (Default: 100)"
CMDPAGELIMIT = "Maximum number of pages to download. This is a numeric value. (Default: 100)"
CMDCOMPRESS = "Enable image compression. This will significantly reduce the size of the images, but will reduce their quality."
CMDTAGSEARCH = "Tag searching mode. Accepts a list of tags separated with spaces."
CMDCONFIG = "Path to a valid config.json file."

# Spanish
CMDHELP_ES = "Muestra éste mensaje y finaliza el programa."
CMDTAGS_ES = "Lista de etiquetas, separadas con espacios (ejemplo: nanakusa_nazuna yofukashi_no_uta)"
CMDFOLDER_ES = "Nombre de la carpeta de salida (donde se guardarán las descargas). Esta carpeta se creará dentro de la carpeta 'collections'. Opcional (Por defecto: default)"
CMDSERVICES_ES = "Lista de servicios de donde descargar el material. Puede tener cualquiera de los siguientes valores: All, gelbooru, safebooru, yandere, danbooru (Default: All)"
CMDSTART_ES = "Página inicial, esto es un valor numérico. Se comenzará la descarga desde ésta página. (Por defecto: 0)"
CMDPOST_PER_PAGE_ES = "Numero de posts por página. De 1 a 100. (Por defecto: 100)"
CMDPAGELIMIT_ES = "Numero máximo de páginas a descargar. Esto es un valor numérico. (Por defecto: 100)"
CMDCOMPRESS_ES = "Activar la compresión de imágenes. Esto reducirá significantemente el tamaño de las imágenes, pero también afectará a la calidad de las mismas."
CMDTAGSEARCH_ES = "Modo búsqueda de etiquetas. Acepta una lista de etiquetas, separadas por espacios (ejemplo: nanakusa_nazuna yofukashi_no_uta)."
CMDCONFIG_ES = "Ruta a un archivo de configuración config.json valido."