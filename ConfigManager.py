import json

# Coinfiguration Manager
class ConfigManager:
    config = {}
    def __init__(self, defaultDir: str, path: str = "./config.json"):
        with open(path, "r") as f:
            self.config = json.loads(f.read())
            for element in self.config.keys():
                if isinstance(self.config[element], str):
                    self.config[element] = self.config[element].replace('{default}/', defaultDir)

            
    def getConfig(self, configval: str):
        if configval in self.config:
            return self.config[configval]
        else:
            return None